import axios from "axios";

class ProviderDataService{

    getProviders(){
        return axios.get('http://localhost:8081/provider/');
    }

    findProviderById(id){
        return axios.get(`http://localhost:8081/provider/${id}`);
    }
    
    updateProvider(id, provider){
        return axios.put(`http://localhost:8081/provider/${id}`, provider);
    }

    createProvider(provider){
        return axios.post('http://localhost:8081/provider/', provider);
    }

    deleteProvider(id){
        return axios.delete(`http://localhost:8081/provider/${id}`);
    }

}

export default new ProviderDataService()