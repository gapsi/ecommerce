import axios from "axios"

class WelcomeService {
    getClient(){
       return axios.get('http://localhost:8081/welcome/client');
    }
    getVersion(){
        return axios.get('http://localhost:8081/welcome/version');
    }

}

export default new WelcomeService()