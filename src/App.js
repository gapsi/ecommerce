import './App.css';
import Header from "./components/Header.jsx";
import Footer from "./components/Footer.jsx";
import Welcome from "./components/Welcome.jsx";
import ProviderList from "./components/ProviderList.jsx";
import Provider from "./components/Provider.jsx";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header/>
      <div className="container"> 
        <Router>
          <>
            <Switch>
              <Route path="/" exact component={Welcome}></Route>
              <Route path="/providers" exact component={ProviderList}></Route>
              <Route path="/providers/:id" exact component={Provider}></Route>
            </Switch>
          </>
        </Router>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
