import { Component } from 'react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import ProviderDataService from '../api/ProviderDataService.js'
import { Link } from 'react-router-dom';

class StoreComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: '',
      businessName: '',
      address: ''
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.inputChangedHandler = this.inputChangedHandler.bind(this);    
  }

  onSubmit(values) {
    let provider = {
      id: this.state.id,
      name: values.name,
      businessName: values.businessName,
      address: values.address
    };
    if (this.state.id === -1 || this.state.id === undefined) {
      ProviderDataService.createProvider(provider).then(() =>
        this.props.history.push('/providers')
      );
    } else {
      ProviderDataService.updateProvider(this.state.id, provider).then(() =>
        this.props.history.push('/providers')
      );
    }
  }

  validate(values) {
    let errors = {};
    if (!values.name) {
      errors.name = 'Nombre obligatorio.';
    }
    return errors;
  }

  componentDidMount() {
    //Review data initialization
    const {id,name,businessName,address} = this.props;
    this.setState({id,name,businessName,address});
    //End review
    if (this.state.id === -1) {
      return;
    }

    ProviderDataService.findProviderById(this.state.id).then((response) =>
      this.setState({
        id: response.data.id,
        name: response.data.name,
        businessName: response.data.businessName,
        address: response.data.address
      })
    );
  }

  inputChangedHandler(event){
    this.setState({
        [event.target.name]: event.target.value
      });
  }



  render() {
    let {id,name,businessName,address} = this.state;
    return (
      <>
        <div className='container mt--7' fluid>
          <Formik
            initialValues={{id,name,businessName,address}}
            onSubmit={this.onSubmit}
            validate={this.validate}
            enableReinitialize={true}
          >
            {(props) => (
              <div className="row">
                <div className='col'>
                    <div className='card shadow'>
                      <div className='card-header bg-white border-0'>
                        <div className='row align-items-center'>
                          <h3 className='mb-0'>Ingresa los datos</h3>
                        </div>
                      </div>
                      <div className="card-body">
                        <Form>
                          <ErrorMessage
                            name='name'
                            component='div'
                            className='alert alert-warning'
                          />
                          <h6 className='heading-small text-muted mb-4'>
                            PROVEEDOR
                          </h6>
                          <div className='pl-lg-4'>
                            <div className='row form-group'>
                              <div className='col-md-4'>
                                <label>Nombre</label>
                                <div className="col-md-4">
                                  <Field
                                    className='form-control'
                                    type='text'
                                    name='name'
                                    onChange={this.inputChangedHandler}
                                  />
                                </div>
                              </div>
                              <div className='col-md-4'>
                                <label>Razón social</label>
                                <div>
                                  <Field
                                    className='form-control'
                                    type='text'
                                    name='businessName'
                                    onChange={this.inputChangedHandler}
                                  />
                                </div>
                              </div>
                              <div className='col-md-4'>
                                <label>Dirección</label>
                                <div>
                                  <Field
                                    className='form-control'
                                    type='text'
                                    name='address'
                                    onChange={this.inputChangedHandler}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <input className='btn btn-success' type='submit' value='Guardar'/> 
                          <Link to='/providers' className='btn btn-warning'>
                            Cancelar
                          </Link>
                        </Form>
                      </div>
                    </div>
                </div>
              </div>
            )}
          </Formik>
        </div>
      </>
    );
  }
}
export default StoreComponent;
