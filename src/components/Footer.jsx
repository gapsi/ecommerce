import React, {Component} from 'react';
import WelcomeService from "../api/WelcomeService.js";

class Footer extends Component {
    constructor(props){
        super(props)
        this.retrieveVersion = this.retrieveVersion.bind(this)
        this.state = {
            version : ''
        }
        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.handleError = this.handleError.bind(this)
    }
    componentDidMount(){
        this.retrieveVersion();
    }

    render() {
        return (
            <div>
                <footer className="nav-footer justify-content-end">
                    <span className="bg-white fixed-bottom text-secondary ">version {this.state.version}</span>
                </footer>
            </div>
        )
    }
    retrieveVersion(){
        WelcomeService.getVersion()
        .then(response => this.handleSuccessResponse(response))
        .catch(error => this.handleError(error))
    }

    handleSuccessResponse(response){
        this.setState({version : response.data})
        
    }

    handleError(error){
        let errorMessage = ''
        if(error.message)
            errorMessage += error.message
        if(error.response && error.response.data){
            errorMessage += error.response.data
        }

        this.setState({client : errorMessage})
        
    }
}

export default Footer