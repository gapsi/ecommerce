import {Component} from 'react';
import { Link } from 'react-router-dom';
import ProviderDataService from "../api/ProviderDataService.js";
import Pages from "../components/Pages.jsx";

class ProviderList extends Component{
    constructor(props){
        super(props)
        this.state = {
            providers: [],
            message : null,
            allProviders: [], 
            currentProviders: [], 
            currentPage: null, 
            totalProviders: null
        }
        this.refreshProviders = this.refreshProviders.bind(this)
        this.addProviderClicked = this.addProviderClicked.bind(this)
        this.updateProviderClicked = this.updateProviderClicked.bind(this)
        this.deleteProviderClicked = this.deleteProviderClicked.bind(this)
    }
    
    componentDidMount(){
        this.refreshProviders();
        
        console.log(this.state.allProviders)
    }

    onPageChanged = data => {
        const { allProviders } = this.state;
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;
        const currentProviders = allProviders.slice(offset, offset + pageLimit);      
        this.setState({ currentPage, currentProviders, totalPages });        
      }

    refreshProviders(){
        ProviderDataService.getProviders()
        .then(
            response => {
                this.setState({ allProviders : response.data })
            }
        )
    }

    addProviderClicked(){
        this.props.history.push('/providers/-1')
    }

    updateProviderClicked(id){
        this.props.history.push(`/providers/${id}`)
    }

    deleteProviderClicked(id){
        ProviderDataService.deleteProvider(id)
        .then(
            response => {
                this.setState({
                    message : `Delete of provider ${id} success`
                })
            }
        )        
        this.props.history.push('/providers')
    }

    render() {        
        const { allProviders, currentProviders, currentPage, totalPages } = this.state;
        const totalProviders = allProviders.length;
        if (totalProviders === 0) return null;
        
        return (
        <>
            <div className="container" fluid>
                <div className="card shadow">
                <div className="card-header border-0">
                    <h3 className="mb-0">Proveedores</h3>
                    <div className="row justify-content-between">
                        <div>
                        <Link className="btn btn-primary" role="button"
                            to={'/providers/-1'} onClick={() => this.addStoreClicked}> 
                            Nuevo
                        </Link>
                        </div>
                        { currentPage && (
                            <div className="col col-6 d-flex justify-content-end">
                                Página { currentPage } / { totalPages }
                            </div>
                            )}
                    </div>
                </div>
                    <table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Razón Social</th>
                                <th scope="col">Dirección</th> 
                                <th scope="col">Acciones</th>                               
                            </tr>
                        </thead>
                        <tbody>
                        {
                            currentProviders.map((provider) =>
                                    <tr key={provider.id} id={provider.id}>                                        
                                        <td>{provider.id}</td>
                                        <td>{provider.name}</td>
                                        <td>{provider.businessName}</td>
                                        <td>{provider.address}</td>        
                                        <td>
                                            <Link className="btn btn-outline-success" role="button"
                                            to={`/providers/${provider.id}`} onClick={() => this.updateProviderClicked(provider.id)}>                                                 
                                            Editar
                                            </Link>
                                            <input type="button" outline='true' className="btn btn-outline-danger" value="Eliminar" onClick={() => this.deleteProviderClicked(provider.id)}/>                                            
                                        </td>                               
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                    <div className="card-footer py-4">
                        <nav aria-label="...">
                            <div className="d-flex flex-row py-4 align-items-center">
                            <Pages totalRecords={totalProviders} pageLimit={2} pageNeighbours={1} onPageChanged={this.onPageChanged} />                           
                            </div>                        
                        </nav>
                    </div>                                        
                </div>
            </div>
        </>
        )
    }

}

export default ProviderList