import React, {Component} from 'react';
import WelcomeService from "../api/WelcomeService.js";
import avatar from "../avatar.png";

class Welcome extends Component {
    constructor(props){
        super(props)
        this.retrieveClient = this.retrieveClient.bind(this)
        this.state = {
            client : ''
        }
        this.handleSuccessResponse = this.handleSuccessResponse.bind(this)
        this.handleError = this.handleError.bind(this)
        this.redirectHome = this.redirectHome.bind(this)
    }
    componentDidMount(){
        this.retrieveClient();
    }

    retrieveClient(){
        WelcomeService.getClient()
        .then(response => this.handleSuccessResponse(response))
        .catch(error => this.handleError(error))
    }

    handleSuccessResponse(response){
        this.setState({client : response.data})
        
    }

    handleError(error){
        let errorMessage = ''
        if(error.message)
            errorMessage += error.message
        if(error.response && error.response.data){
            errorMessage += error.response.data
        }

        this.setState({client : errorMessage})
        
    }

    redirectHome(){
        this.props.history.push(`/providers`)
    }

    render() {
        return (
            <>
                <h1>Home</h1>
                <div className="container">
                    <div>
                        <img src={avatar} alt="logo" className="profile"/>
                    </div>
                    <div>
                        Bienvenido : {this.state.client}.
                    </div>                                    
                </div>
                <div className="container">
                    <button onClick={this.redirectHome} className="btn btn-primary">Continuar</button>
                </div>
            </>
        )
    }
    
}

export default Welcome