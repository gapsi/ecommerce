import React, {Component} from 'react';
import logo from "../logo.png";

class Header extends Component {
    render() {

        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-light">
                    <div><a href="http://grupoasesores.com.mx/" className="navbar-brand"><img src={logo} alt="logo" /></a></div>
                    <ul className="navbar-nav">
                        <li>e-Commerce Gapsi</li>
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">                       
                        <li><a href="/logout" className="nav-link text-secondary" >Salir</a></li>
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header;